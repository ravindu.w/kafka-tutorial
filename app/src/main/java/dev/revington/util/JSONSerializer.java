/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.revington.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.apache.kafka.common.serialization.Serializer;
import org.json.JSONObject;

/**
 *
 * @author User
 */
public class JSONSerializer implements Serializer<JSONObject> {

    @Override
    public byte[] serialize(String topic, JSONObject data) { 
        return data.toString().getBytes();
    }
 
}
