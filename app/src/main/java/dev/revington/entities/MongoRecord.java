/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.revington.entities;

import org.json.JSONObject;

/**
 *
 * @author User
 */
public class MongoRecord {
    
    private JSONObject _id;
    private String operationType;
    private JSONObject fullDocument;
    private JSONObject ns;
    private JSONObject to;
    private JSONObject documentKey;
    private JSONObject updateDescription;
    private JSONObject clusterTime;
    private JSONObject txnNumber;
    private JSONObject lsid;

    public JSONObject getId() {
        return _id;
    }

    public void setId(JSONObject _id) {
        this._id = _id;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public JSONObject getFullDocument() {
        return fullDocument;
    }

    public void setFullDocument(JSONObject fullDocument) {
        this.fullDocument = fullDocument;
    }

    public JSONObject getNs() {
        return ns;
    }

    public void setNs(JSONObject ns) {
        this.ns = ns;
    }

    public JSONObject getTo() {
        return to;
    }

    public void setTo(JSONObject to) {
        this.to = to;
    }

    public JSONObject getDocumentKey() {
        return documentKey;
    }

    public void setDocumentKey(JSONObject documentKey) {
        this.documentKey = documentKey;
    }

    public JSONObject getUpdateDescription() {
        return updateDescription;
    }

    public void setUpdateDescription(JSONObject updateDescription) {
        this.updateDescription = updateDescription;
    }

    public JSONObject getClusterTime() {
        return clusterTime;
    }

    public void setClusterTime(JSONObject clusterTime) {
        this.clusterTime = clusterTime;
    }

    public JSONObject getTxnNumber() {
        return txnNumber;
    }

    public void setTxnNumber(JSONObject txnNumber) {
        this.txnNumber = txnNumber;
    }

    public JSONObject getLsid() {
        return lsid;
    }

    public void setLsid(JSONObject lsid) {
        this.lsid = lsid;
    }
    
}
