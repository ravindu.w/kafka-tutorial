FROM confluentinc/cp-kafka-connect:7.2.2

COPY connectors .
CMD [ "bash", "-c", "sleep infinity" ]