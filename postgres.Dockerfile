FROM postgres:latest

WORKDIR /home/user
COPY sql . 

RUN apt-get update
RUN apt-get install -f -y git make gcc
RUN apt-get install -f -y postgresql postgresql-contrib libpq-dev software-properties-common build-essential pkg-config postgresql-server-dev-$PG_MAJOR libproj-dev 
RUN add-apt-repository "deb http://ftp.debian.org/debian testing main contrib" &&  apt-get update && apt-get install -f -y --no-install-recommends libprotobuf-c-dev
RUN git clone https://github.com/debezium/postgres-decoderbufs --single-branch \
    && cd postgres-decoderbufs \
    && make && make install \
    && cd .. \
    && rm -rf postgres-decoderbufs 
RUN git clone https://github.com/eulerto/wal2json -b master --single-branch \
    && cd wal2json \
    && git checkout $WAL2JSON_COMMIT_ID \
    && make && make install \
    && cd .. \
    && rm -rf wal2json 