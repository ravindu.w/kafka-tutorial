FROM gradle:jdk17-alpine AS BUILD_JAR

COPY . /home/gradle/stream 
WORKDIR /home/gradle/stream 
CMD gradle run
#RUN gradle shadowJar --no-daemon
#WORKDIR /home/gradle/stream/app/build/libs 
#RUN mkdir /app

#FROM openjdk:17-alpine
#RUN mkdir /usr/app
#COPY --from=BUILD_JAR /home/gradle/stream/app/build/libs/app-all.jar /usr/app
#CMD java -jar /usr/app/app-all.jar